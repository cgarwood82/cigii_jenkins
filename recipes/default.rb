#
# Cookbook:: cigii_jenkins
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

include_recipe 'jenkins::master'

# Remove xsite vuln
jenkins_script 'csrf.groovy' do
  command <<-EOH.gsub(/^ {4}/, '')
    import hudson.security.csrf.DefaultCrumbIssuer
    import jenkins.model.Jenkins

    def instance = Jenkins.instance
    instance.setCrumbIssuer(new DefaultCrumbIssuer(true))
    instance.save()
  EOH
end

jenkins_plugins = %w(
  credentials
  chef-identity
  pipeline-stage-view
  workflow-aggregator
  ssh-steps
  ssh-agent
)

jenkins_plugins.each do |plugin|
  jenkins_plugin plugin do
    notifies :execute, 'jenkins_command[safe-restart]', :immediately
  end
end

# Create a user
jenkins_password_credentials 'trans' do
  id 'trans-password'
  description 'trans user'
  password 'password'
end

jenkins_command 'safe-restart' do
  action :nothing
end
